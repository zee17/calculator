var express = require('express');
var router = express.Router();
var res = require("ejs");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('calculator.ejs',{result:''}, function(err, result) {
	   // render on success
	   if (!err) {
	            res.end(result);
	   }
	   // render or error
	   else {
	            res.end('An error occurred');
	            console.log(err);
	   }
   });
});
router.post('/', function(req, res, next) {
	//res.end(JSON.stringify(req.body))

	var Num1 = parseInt(req.body.Num1);
	var Num2 = parseInt(req.body.Num2);
	var Operation = req.body.Operation;
	if(Operation == '+')
	{
		var result = Num1+Num2;
	}
	else if(Operation == '-')
	{
		var result = Num1-Num2;
	}
	else if(Operation == '*')
	{
		var result = Num1*Num2;
	}
	else if(Operation == '/')
	{
		if(Num2 == 0)
		{
			var result = 0;
		}
		else
		{
			var result = Num1/Num2;
		}
	}
	else
	{
		var result = "Invalid operator. Please enter +, -, * or / only!";
	}
	
  res.render('calculator', { result: result });
   });
 

module.exports = router;
